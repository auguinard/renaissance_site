import {combineReducers} from "redux";
import forumPostList from "./reducers/forumPostList";
import forumPost from "./reducers/forumPost";
import articlePostList from "./reducers/articlePostList";
import articlePost from "./reducers/articlePost";
import titrologieList from "./reducers/titrologieList";
import forumCommentList from "./reducers/forumCommentList";
import {reducer as formReducer} from 'redux-form';
import auth from "./reducers/auth";
import galerieList from "./reducers/galerieList";
import forumReplyList from "./reducers/forumReplyList";

export default combineReducers({
   forumPostList,
   forumPost,
   forumCommentList,
   forumReplyList,
   articlePostList,
   articlePost,
   titrologieList,
   galerieList,
   auth,
   form: formReducer
});