import {
    FORUM_POST_LIST_ADD,
    FORUM_POST_LIST_ERROR,
    FORUM_POST_LIST_RECEIVED,
    FORUM_POST_LIST_REQUEST
} from "../actions/constants";

export default (state = {
    posts: null,
    isFetching: false
}, action) => {
    switch (action.type) {
        case FORUM_POST_LIST_REQUEST:
            state = {
                ...state,
                isFetching: true
            };
            return state;
        case FORUM_POST_LIST_RECEIVED:
            state = {
                ...state,
                posts: action.data['hydra:member'],
                isFetching: false
            };
            return state;
        case FORUM_POST_LIST_ERROR:
            state = {
                ...state,
                isFetching: false,
                posts: null
            }
            return state;
        case FORUM_POST_LIST_ADD:
            state = {
                ...state,
                posts: state.posts ? state.posts.concat(action.data) : state.posts
            };
            return state;
        default:
            return state;
    }
}