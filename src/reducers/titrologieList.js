import {
    TITROLOGIE_ERROR,
    TITROLOGIE_RECEIVED,
    TITROLOGIE_REQUEST

} from "../actions/constants";

export default (state = {
    revues: null,
    isFetching: false
}, action) => {
    switch (action.type) {
        case TITROLOGIE_REQUEST:
            state = {
                ...state,
                isFetching: true
            };
            return state;
        case TITROLOGIE_RECEIVED:
            state = {
                ...state,
                revues: action.data['hydra:member'],
                isFetching: false
            };
            return state;
        case TITROLOGIE_ERROR:
            state = {
                ...state,
                isFetching: false,
                revues: null
            };
            return state;
        default:
            return state;
    }
}