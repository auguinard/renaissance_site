import {
    FORUM_COMMENT_LIST_ERROR,
    FORUM_COMMENT_LIST_RECEIVED,
    FORUM_COMMENT_LIST_REQUEST,
    FORUM_COMMENT_LIST_UNLOAD
} from "../actions/constants";

export default (state = {
    commentList: null,
    isFetching: false
}, action) => {
    switch (action.type) {
        case FORUM_COMMENT_LIST_REQUEST:
            return {
                ...state,
                isFetching: true
            };
        case FORUM_COMMENT_LIST_RECEIVED:
           return  {
                ...state,
                commentList: action.data['hydra:member'],
                isFetching: false
            };
        case FORUM_COMMENT_LIST_ERROR:
        case FORUM_COMMENT_LIST_UNLOAD:
            return  {
                ...state,
                isFetching: false,
                commentList: null
            }
        default:
            return state;
    }
}