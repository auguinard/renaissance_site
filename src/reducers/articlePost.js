import {
    ARTICLE_POST_ERROR,
    ARTICLE_POST_RECEIVED,
    ARTICLE_POST_REQUEST,
    ARTICLE_POST_UNLOAD

} from "../actions/constants";

export default (state = {
    article: null,
    isFetching: false
}, action) => {
    switch (action.type) {
        case ARTICLE_POST_REQUEST:
            return {
                ...state,
                isFetching: true
            };
        case ARTICLE_POST_RECEIVED:
            return {
                ...state,
                article: action.data,
                isFetching: false
            };
        case ARTICLE_POST_ERROR:
            return {
                ...state,
                isFetching: false
            };
        case ARTICLE_POST_UNLOAD:
            return {
                ...state,
                isFetching: false,
                article: null
            }
        default:
            return state;
    }
}