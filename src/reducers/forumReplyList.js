import {
    FORUM_REPLY_LIST_ERROR,
    FORUM_REPLY_LIST_RECEIVED,
    FORUM_REPLY_LIST_REQUEST,
    FORUM_REPLY_LIST_UNLOAD

} from "../actions/constants";

export default (state = {
    replyList: null,
    isFetching: false
}, action) => {
    switch (action.type) {
        case FORUM_REPLY_LIST_REQUEST:
            return {
                ...state,
                isFetching: true
            };
        case FORUM_REPLY_LIST_RECEIVED:
            return  {
                ...state,
                replyList: action.data['hydra:member'],
                isFetching: false
            };
        case FORUM_REPLY_LIST_ERROR:
        case FORUM_REPLY_LIST_UNLOAD:
            return  {
                ...state,
                isFetching: false,
                replyList: null
            }
        default:
            return state;
    }
}