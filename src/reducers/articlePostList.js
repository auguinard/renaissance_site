import {
    ARTICLE_POST_LIST_ERROR,
    ARTICLE_POST_LIST_RECEIVED,
    ARTICLE_POST_LIST_REQUEST, ARTICLE_POST_LIST_SET_PAGE
} from "../actions/constants";

export default (state = {
    articles: null,
    isFetching: false,
    currentPage: 1,
    pageCount: null
}, action) => {
    switch (action.type) {
        case ARTICLE_POST_LIST_REQUEST:
            state = {
                ...state,
                isFetching: true
            };
            return state;
        case ARTICLE_POST_LIST_RECEIVED:
            state = {
                ...state,
                articles: action.data['hydra:member'],
                isFetching: false
            };
            return state;
        case ARTICLE_POST_LIST_ERROR:
            state = {
                ...state,
                isFetching: false,
                articles: null
            };
            return state;
        case ARTICLE_POST_LIST_SET_PAGE:
            state = {
                ...state,
                currentPage: action.page
            };
            return state;
        default:
            return state;
    }
}