import {
    FORUM_POST_ERROR,
    FORUM_POST_RECEIVED,
    FORUM_POST_REQUEST,
    FORUM_POST_UNLOAD
} from "../actions/constants";

export default (state = {
    post: null,
    isFetching: false
}, action) => {
    switch (action.type) {
        case FORUM_POST_REQUEST:
            return {
                ...state,
                isFetching: true
            };
        case FORUM_POST_RECEIVED:
            return {
                ...state,
                post: action.data,
                isFetching: false
            };
        case FORUM_POST_ERROR:
            return {
                ...state,
                isFetching: false
            };
        case FORUM_POST_UNLOAD:
            return {
                ...state,
                isFetching: false,
                post: null
            }
        default:
            return state;
    }
}