import {
    GALERIE_ERROR,
    GALERIE_RECEIVED,
    GALERIE_REQUEST

} from "../actions/constants";

export default (state = {
    photos: null,
    isFetching: false
}, action) => {
    switch (action.type) {
        case GALERIE_REQUEST:
            state = {
                ...state,
                isFetching: true
            };
            return state;
        case GALERIE_RECEIVED:
            state = {
                ...state,
                photos: action.data['hydra:member'],
                isFetching: false
            };
            return state;
        case GALERIE_ERROR:
            state = {
                ...state,
                isFetching: false,
                revues: null
            };
            return state;
        default:
            return state;
    }
}