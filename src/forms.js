import React from 'react';

export const renderField = ({input, label, placeholder, type, meta: {error}}) => {
    return(
        <div className="finput-group mb-3">
            {label !== null && label !== '' && <label>{label}</label> }
            {type !== 'textarea' && <input {...input} type={type} placeholder={placeholder} className="form-control input_user"/>}
            {type === 'textarea' && <input {...input} className="form-control"/>}
        </div>
    );
};