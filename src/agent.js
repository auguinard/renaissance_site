import superagentPromise from 'superagent-promise';
import _superagent from 'superagent';

const API_ROOT = 'http://localhost:8000/api';
const superagent = superagentPromise(_superagent, global.Promise)
const responseBody = response => response.body;

let token = null;

const tokenPlugins = secured => {
   return (request) => {
        if (token && secured){
            request.set('Authorization', `Bearer ${token}`);
        }
    };
};


export const requests = {
    get: (url, secured = false) =>
        superagent.get(`${API_ROOT}${url}`).use(tokenPlugins(secured)).then(responseBody),
    post: (url, body = null, headers = null, secured = true) =>
        superagent.post(`${API_ROOT}${url}`).set('Content-Type', 'application/json').use(tokenPlugins(secured)).then(responseBody),
    setToken: (newJwtToken) => token = newJwtToken
};



