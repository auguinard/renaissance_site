import React from 'react';
import ForumPostList from "./ForumPostList";
import {forumPostAdd, forumPostListFetch} from "../actions/actions";
import {connect} from "react-redux";
import {Spinner} from "./Spinner";
import {Message} from "./Message";


const mapStateToProps = state => ({
    ...state.forumPostList
});

const mapDispatchToProps = {
    forumPostAdd,
    forumPostListFetch
}

class ForumPostListContainer extends React.Component{
    componentDidMount() {
        this.props.forumPostListFetch();
        document.title = 'Rénaissance | Galérie';
    }



    render(){
        const {posts, isFetching} = this.props;
        if (null === posts || 0 === posts.length){
            return (<Message message="Aucun sujet pour l'instant"/>);
        }
        if (isFetching) {
            return (
                <div className="container mt-5">
                    <br/>
                    <br/>
                    <br/>
                    <Spinner/>
                </div>
            );
        }
        return (
            <ForumPostList posts={posts} />
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ForumPostListContainer);