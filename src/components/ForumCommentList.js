import React from 'react';
import {Message} from "./Message";
import timeago from 'timeago.js';

export class ForumCommentList extends React.Component{

    render() {
        const {commentList} =  this.props;
        //console.log(commentList.length);
        if (null === commentList){
            return (<Message message="Not Comment yet!" />);
        }

        return (
            <div className="card mb-3 mt-3 shadow-sm">
                { commentList.map(comment => {
                    return (
                        <div className="card-body" key={comment.id}>
                            <p className="card-text mb-0">
                                {comment.content}
                            </p>
                            <p className="card-text">
                                <small className="text-muted">
                                    {timeago().format(comment.dateAjout, 'fr_FR')} par&nbsp;
                                    {comment.auteur.username}
                                </small>
                            </p>
                        </div>

                    );
                }) }
            </div>
        )
    }
}