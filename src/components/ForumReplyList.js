import React from 'react';
import {Message} from "./Message";
import timeago from 'timeago.js';

export class ForumReplyList extends React.Component{
    render() {
        const {replyList} =  this.props;
        console.log(replyList);
        if (null === replyList){
            return (<Message message="Not Comment yet!" />);
        }

        return (
            <div className="card mb-3 mt-3 shadow-sm">
                { replyList.map(replyLis => {
                    return (
                        <div className="card-body" key={replyLis.id}>
                            <p className="card-text mb-0">
                                {replyLis.content}
                            </p>

                            <p className="card-text">
                                <small className="text-muted">
                                    {timeago().format(replyLis.dateAjout)} par&nbsp;
                                    {replyLis.auteur.username}
                                </small>
                            </p>
                        </div>
                    );
                }) }
            </div>
        )
    }
}