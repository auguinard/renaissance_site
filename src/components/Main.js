import React from 'react';
import "./Main.css";
import Footer from "./Footer";

export default class Main extends React.Component {

    render() {
        return(
            <section className="">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-lg-9">
                            <div className="container">
                                <div className="">
                                    <section id="about" className="section pt-0 pb-2">
                                        <h2 className="text-center white-text text-uppercase font-weight-bold mt-5 mt-4 wow fadeIn" data-wow-delay="0.2s">
                                            Notre vision
                                        </h2>
                                        <div className="line wow fadeIn" data-wow-delay="0.2s"></div>
                                        <div className="row mt-5 pt-2">
                                            <div className="col-lg-3 col-md-12 mb-5 mt-lg-4 wow fadeIn" data-wow-delay="0.4s">
                                                <img src="https://nsm09.casimages.com/img/2020/07/05/20070501455725536216904571.jpg" className="img-thumbnail" alt="my"/>
                                            </div>
                                            <div className="col-lg-8 col-md-12 wow fadeIn" data-wow-delay="0.4s">
                                                <p className="grey-text mb-4" align="justify">
                                                    Renaissance réaffirme que la Côte d'Ivoire est une terre indivisible. Notre ambition pour ce pays, est de construire un État de droit respectueux des droits et libertés de la personne humaine, un pays caractérisé par une démocratie pluraliste, consensuelle et une justice indépendante, un pays sans discrimination aucune, qui privilégie le dialogue et la réconciliation, veille au respect mutuel entre les ivoiriennes et Ivoiriens eux-mêmes et autres peuples, un pays qui privilégie le développement, intégré, l’équité et l’égalité des chances, un pays ou l’espace politique est ouvert, où les gouvernants rendent compte au peuple.
                                                </p>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                                <div className="">
                                    <section id="about" className="section pt-0 pb-2">
                                        <h2 className="text-center white-text text-uppercase font-weight-bold mt-5 mt-4 wow fadeIn" data-wow-delay="0.2s">
                                            Nos Objectifs
                                        </h2>
                                        <div className="line wow fadeIn" data-wow-delay="0.2s"></div>
                                        <div className="row mt-5 pt-2">
                                            <div className="col-lg-8 col-md-12 wow fadeIn" data-wow-delay="0.4s">
                                                <p className="grey-text mb-4" align="justify">
                                                    Mettre en place des mécanismes constitutionnels et des arrangements institutionnels de gestion du pouvoir qui rassureraient et sécuriseraient tout un chacun pour mettre fin aux différentes formes de rébellions qui en découlent. Faire du critère de la compétence, la seule norme qui inspire et oriente les nominations aux hautes fonctions de l’État et dans la gestion du pays Créer un observatoire indépendant de droits de l’homme. Faire de la lutte contre la corruption une priorité nationale.
                                                </p>
                                            </div>
                                            <div className="col-lg-3 col-md-12 mb-5 mt-lg-2 wow fadeIn" data-wow-delay="0.4s">
                                                <img src="https://nsm09.casimages.com/img/2020/07/05/20070501535425536216904606.jpg" className="img-thumbnail" alt="My quelque chose"/>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                                <div className="">
                                    <section id="about" className="section pt-0 pb-2">
                                        <h2 className="text-center white-text text-uppercase font-weight-bold mt-5 mt-4 wow fadeIn" data-wow-delay="0.2s">
                                            Notre stratégie
                                        </h2>
                                        <div className="line wow fadeIn" data-wow-delay="0.2s"></div>
                                        <div className="row mt-5 pt-2">
                                            <div className="col-lg-4 col-md-12 mb-5 mt-lg-2 wow fadeIn" data-wow-delay="0.4s">
                                                <img src="https://nsm09.casimages.com/img/2020/07/05/20070501562925536216904610.jpg" className="img-fluid" alt="My nouveau"/>
                                            </div>
                                            <div className="col-lg-8 col-md-12 wow fadeIn" data-wow-delay="0.4s">
                                                <p className="grey-text mb-4" align="justify">
                                                    Mettre en place des mécanismes constitutionnels et des arrangements institutionnels de gestion du pouvoir qui rassureraient et sécuriseraient tout un chacun pour mettre fin aux différentes formes de rébellions qui en découlent. Faire du critère de la compétence, la seule norme qui inspire et oriente les nominations aux hautes fonctions de l’État et dans la gestion du pays Créer un observatoire indépendant de droits de l’homme. Faire de la lutte contre la corruption une priorité nationale.
                                                </p>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                                <div className="">
                                    <section id="about" className="section pt-0 pb-2">
                                        <h2 className="text-center white-text text-uppercase font-weight-bold mt-5 mt-4 wow fadeIn" data-wow-delay="0.2s">
                                            Avant tout et surtout
                                        </h2>
                                        <div className="line wow fadeIn" data-wow-delay="0.2s"></div>
                                        <div className="row mt-5 pt-2">
                                            <div className="col-lg-8 col-md-12 wow fadeIn" data-wow-delay="0.4s">
                                                <p className="grey-text mb-4" align="justify">
                                                    Notre conviction est que la diversité constitue une richesse inestimable, que la vie humaine est sacrée, que personne ne doit être victime ni de sa religion, ni de ses origines et/ou de son passé, ni de son ethnie, ni enfin de son opinion politique. De ce fait, nous nous engageons dans un processus qui devra conduire vers un renouveau démocratique d’un peuple réconcilié, où la citoyenneté est une réalité dans les institutions et dans tous les secteurs de la vie du pays où le choix politique se fait autour des idées et de la communauté d’intérêts au lieu de mettre en avant l’appartenance ethnique, clanique, régionale ou autres. Nous considérons que la crise que traverse la Côte d'Ivoire depuis plusieurs décennies, ne peut se régler que par une solution politique négociée, par le dialogue et l’ouverture politique. D’où la nécessité d’instaurer une commission plurielle, chargé de mettre en place et conduire ce processus de réconciliation, d’imaginer les mécanismes pour préserver la paix et la concorde nationale. Nous considérons qu’il faut créer les conditions d’un climat politique et social qui permette de mettre fin à cette tendance récurrente d’une logique d’accéder au pouvoir par la violence. Nous croyons qu’il faut créer les conditions d’un véritable état de Droit et prendre toutes mesures utiles qui permettent de doter le pays, d’institutions républicaines fortes.
                                                </p>
                                            </div>
                                            <div className="col-lg-3 col-md-12 mb-5 mt-lg-5 wow fadeIn" data-wow-delay="0.4s">
                                                <img src="https://nsm09.casimages.com/img/2020/07/05/20070501515225536216904603.jpg" className="img-thumbnail" alt="My Encore"/>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-3">
                            <div className="card blue-grey lighten-5 mt-5 mb-2 my-4">
                                <div className="flex-center mt-1 bio">
                                    <center>
                                        <img src="https://nsm09.casimages.com/img/2020/07/05/20070511582425536216904386.jpg"
                                             className="img-fluid img-author z-depth-1" alt="vf"/>
                                    </center>
                                        <a>
                                            <div className="mask rgba-white-slight"></div>
                                        </a>
                                </div>

                                <div className="card-body">
                                    <h5 className="card-title dark-grey-text text-center grey lighten-4 py-2">
                                        <strong>
                                            OLI ALEXIS OLI
                                        </strong>
                                    </h5>
                                    <p className="mt-3 dark-grey-text font-small text-center" align="justify"><em>
                                        <small>
                                            Diplômé de l'université FHB de Cocody en Côte d'Ivoire, option Psychologie sociale, Diplômé de l'université de Paris X. Nanterre en Sciences de l'éducation familiale. Président de l'association ivoirienne des étudiants en psychologie de 1994 à 1998 et Président de l'association des jeunes psychologues de Côte d'Ivoire a la même période. Président de l'association des ressortissants de la sous-préfecture de Nahio en France (ARSNAF) de 2006 à 2009, Cadre du social.
                                        </small>
                                    </em></p>
                                </div>
                            </div>
                            <div className="card blue-grey mt-2 mb-2">
                                <div className="card-body">
                                    <h5 className="card-title dark-grey-text text-center grey py-1">
                                        <strong>
                                            Qui somme nous
                                        </strong>
                                    </h5>
                                    <p className="mt-3 text-center" align="justify">
                                        Nous somme un mouvement politique transpartisan. De ce fait il est ouvert à toute personne, à toute organisation qui adhère à sa vision et ses valeurs sont donc le principal centre d’intérêt reste la Reconstruction de la Côte d’Ivoire et le bien-être de la population ivoirienne.
                                    </p>
                                </div>
                            </div>
                            <div className="card blue-grey mt-2 mb-2 my-4">
                                <div className="card-body">
                                    <h5 className="card-title dark-grey-text text-center grey lighten-4 py-2">
                                        <strong>
                                            Revue de presse ivoirienne
                                        </strong>
                                    </h5>
                                    <div id="carousel-example-4" class="carousel slide carousel-fade z-depth-1-half" data-ride="carousel">
                                        <ol class="carousel-indicators">
                                            <li data-target="#carousel-example-4" data-slide-to="0" class="active"></li>
                                            <li data-target="#carousel-example-4" data-slide-to="1"></li>
                                            <li data-target="#carousel-example-4" data-slide-to="2"></li>
                                        </ol>

                                        <div class="carousel-inner" role="listbox">
                                            <div class="carousel-item active">
                                                <div class="view">
                                                    <img src="https://nsm09.casimages.com/img/2020/07/05/20070502253225536216904708.jpg" class="img-fluid" alt="jjd"/>
                                                        <a href="">
                                                            <div class="mask flex-center rgba-black-light"></div>
                                                        </a>
                                                </div>
                                            </div>

                                            <div class="carousel-item">
                                                <div class="view">
                                                    <img src="https://nsm09.casimages.com/img/2020/07/05/20070502253325536216904709.jpg" class="img-fluid" alt="djjd"/>
                                                        <a href="">
                                                            <div class="mask flex-center rgba-black-light"></div>
                                                        </a>
                                                </div>
                                            </div>

                                            <div class="carousel-item">
                                                <div class="view">
                                                    <img src="https://nsm09.casimages.com/img/2020/07/05/20070502253325536216904710.jpg" class="img-fluid" alt=" jh"/>
                                                        <a href="">
                                                            <div class="mask flex-center rgba-black-light"></div>
                                                        </a>
                                                </div>
                                            </div>
                                        </div>

                                        <a class="carousel-control-prev" href="#carousel-example-4" role="button" data-slide="prev">
                                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                            <span class="sr-only">Previous</span>
                                        </a>
                                        <a class="carousel-control-next" href="#carousel-example-4" role="button" data-slide="next">
                                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                            <span class="sr-only">Next</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div className="card blue-grey mt-2 mb-5">
                                <div className="card-body">
                                    <h5 className="card-title dark-grey-text text-center grey py-1">
                                        <strong>
                                            Rejoignez notre bulletin d'information
                                        </strong>
                                    </h5>
                                    <div className="row mt-2">
                                        <div className="col-md-12">
                                            <div className="input-group md-form form-sm form-3 pl-0">
                                                <span className="input-group-text white black-text" id="basic-addon9">
                                                    @
                                                </span>
                                                <input type="email"
                                                       className="form-control mt-0 black-border rgba-white-strong"
                                                       placeholder="Email" aria-describedby="basic-addon9"/>
                                            </div>
                                            <button type="button" className="btn btn-grey btn-block mt-4" id="Nletter">
                                                Envoyer
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <Footer/>
            </section>
        );
    }
}