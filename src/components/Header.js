import React from 'react';
import "./Header.css";
import NavBar from "./NavBar";
export default class Header extends React.Component {
    render() {
        return(
            <div>
            <NavBar/>
            <section className="masthead view jarallax" data-jarallax='{"speed": 0.2}'>
                <div className="rgba-black-slight h-100 d-flex justify-content-center align-items-center">
                    <div className="container">
                        <div className="jumbotron mt-2">
                            <div className="white-text text-center">
                                <h2 id="slogan1" className="display-5 font-weight-bold mt-4 wow fadeIn">
                                    <center>
                                        Bienvenue sur renaissance.ci
                                    </center>
                                </h2>
                                <h4 id="slogan" className="indigo-text font-weight-bold my-5 wow fadeIn" data-wow-delay="0.2s">
                                    <small className="font-weight-bold">La côte d'Ivoire, nôtre bien commun</small>
                                </h4>
                                <a data-offset="30" className="btn btn-outline-white mb-5">
                                    Dévenir membre
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            </div>
        );
    }
}