import React from 'react';
import {
    forumReplyListFetch, forumReplyListUnload
}
    from "../actions/actions";
import {connect} from "react-redux";
import {Spinner} from "./Spinner";
import {ForumReplyList} from "./ForumReplyList";

const mapeStateToProps = state => ({
    ...state.forumReplyList
});

const mapDispatchToProps = {
    forumReplyListFetch,
    forumReplyListUnload
}

class ForumCommentListContainer extends React.Component{
    componentDidMount() {
        this.props.forumReplyListFetch(this.props.forumCommentId);
    }

    componentWillUnmount() {
        this.props.forumReplyListUnload();
    }

    render() {
        const {isFetching, replyList} = this.props;
        if (isFetching) {
            return (
                <div className="container mt-5">
                    <br/>
                    <br/>
                    <br/>
                    <Spinner/>
                </div>
            );
        }
        return(
            <div>
                <ForumReplyList replyList={replyList} />
            </div>
        )
    }
}

export default connect(mapeStateToProps, mapDispatchToProps)(ForumCommentListContainer);