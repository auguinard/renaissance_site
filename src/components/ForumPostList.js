import React from 'react';
import NavBar from "./NavBar";
import Footer from "./Footer";
import "./ForumPostList.css";
import timeago from 'timeago.js';
import {Link} from "react-router-dom";

class ForumPostList extends React.Component{
    render(){
        const {posts} =  this.props;
        return (
            <div>
                <NavBar/>
                    <section className="container">
                    <div className="mt-5 mb-5">
                        <div className="row">
                            <div className="col-lg-8">
                                <br/>
                                <nav className="navbar navbar-expand-lg navbar-light bg-light mt-5" >
                                    <b className="font-weight-bold white-text mr-4" href="/forum">
                                        Accueil Forum
                                    </b>
                                    <button className="navbar-toggler" type="button" data-toggle="collapse"
                                            data-target="#navbarSupportedContent1"
                                            aria-controls="navbarSupportedContent1" aria-expanded="false"
                                            aria-label="Toggle navigation">
                                        <span className="navbar-toggler-icon"></span>
                                    </button>
                                    <div className="collapse navbar-collapse" id="navbarSupportedContent1">
                                        <ul className="navbar-nav mr-auto">

                                        </ul>

                                        <form className="search-form" role="search">
                                            <div className="form-group md-form my-0 waves-light">
                                                <input type="text" className="form-control" placeholder="Recherche..." name='q' />
                                            </div>
                                        </form>
                                    </div>
                                </nav>
                                <hr/>
                                <div>
                                    {posts && posts.map(post => (
                                        <div  className="card mb-3 mt-2 shadow-sm" key={post.id}>
                                            <div className="card-body">
                                                <h3>
                                                    <Link to={`/forum/${post.id}`}>
                                                        {post.titre}
                                                    </Link>
                                                </h3>
                                                <p className="card-text bordet-top">
                                                    <small className="text-muted">
                                                        {timeago().format(post.dateAjout)}
                                                    </small>
                                                </p>
                                            </div>
                                        </div>
                                    ))}
                                </div>
                            </div>
                            <div className="col-lg-4">
                                <div className="card blue-grey mt-5 mb-3">
                                    <h5 className="card-title dark-grey-text text-center grey lighten-4 mt-4">
                                        <strong>
                                            Les règles à respecter
                                        </strong>
                                    </h5>
                                    <div className="card-body">
                                        <div className="card-body">
                                            <ul className="list-unstyled">
                                                <li>
                                                    <span> - Rédiger les messages dans un langage clair sans abréviations.</span>
                                                </li>
                                                <li>
                                                    <span> - Faire usage de formule de politesse et échanger avec courtoisie.</span>
                                                </li>

                                                <li>
                                                    <span> - Réserver un accueil cordial aux nouveaux utilisateurs.</span>
                                                </li>
                                                <li>
                                                    <span> - Poster son message dans le thème le plus approprié.</span>
                                                </li>
                                                <li>
                                                    <span> - Respecter la législation en vigueur.</span>
                                                </li>
                                            </ul>
                                            <p className="text-danger"><i className="fas fa-ban"></i> <em>Attention,
                                                toutes publications ne respectant pas l'une des règles ci-dessus sera
                                                immédiatement supprimer</em></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <Footer/>
            </div>
        )
    }
}

export default ForumPostList;