import React from 'react';
import {Spinner} from "./Spinner";
import {ArticlePost} from "./ArticlePost";
import {connect} from "react-redux";
import {articlePostFetch, articlePostUnload} from "../actions/actions";

const mapStateToProps = state => ({
    ...state.articlePost
});

const mapDispatchToProps = {
    articlePostFetch,
    articlePostUnload
}

class ArticlePostContainer extends React.Component{
    componentDidMount() {
        this.props.articlePostFetch(this.props.match.params.id);
        document.title = 'Rénaissance | Détails aricles';
    }

    componentWillMount() {
        this.props.articlePostUnload();
    }

    render() {
        const {isFetching, article} = this.props;
        if (isFetching){
            return (
                <div className="container mt-5">
                    <br/>
                    <br/>
                    <br/>
                    <Spinner/>
                </div>
            );
        }
        return (
            <div>
                <ArticlePost article={article} />
            </div>
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ArticlePostContainer);