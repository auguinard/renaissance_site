import React from 'react';
import {
    forumCommentListFetch,
    forumCommentListUnload}
    from "../actions/actions";
import {connect} from "react-redux";
import {Spinner} from "./Spinner";
import {ForumCommentList} from "./ForumCommentList";
import FcommentForm from "./FcommentForm";

const mapeStateToProps = state => ({
    ...state.forumCommentList,
    //isAuthenticated: state.auth.isAuthenticated
});

const mapDispatchToProps = {
    forumCommentListFetch,
    forumCommentListUnload
}

class ForumCommentListContainer extends React.Component{
    componentDidMount() {
        this.props.forumCommentListFetch(this.props.forumPostId);
    }

    componentWillUnmount() {
        this.props.forumCommentListUnload();
    }

    render() {
        const {isFetching, commentList, forumPostId} = this.props;
        if (isFetching) {
            return (
                <div className="container mt-5">
                    <br/>
                    <br/>
                    <br/>
                    <Spinner/>
                </div>
            );
        }
        return(
            <div>
                <ForumCommentList commentList={commentList} />
                <FcommentForm forumPostId={forumPostId}/>
            </div>
        )
    }
}

export default connect(mapeStateToProps, mapDispatchToProps)(ForumCommentListContainer);