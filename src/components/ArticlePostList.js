import React from 'react';
import {Message} from "./Message";
import {Link} from "react-router-dom";
import './Article.css';

class ArticlePostList extends React.Component{
    render() {
        const {articles} = this.props;
        if (null === articles || 0 === articles.length){
            return (<Message message="Pas d'actualités pour l'instant"/>);
        }
        return (
            <div>
                {articles && articles.map(article => (
                <div className="card blue-grey mt-5 mb-5" key={article.id}>
                    <div className="card-body">
                        <div className="row">
                            <div className="col-md-5 mx-3 my-3">
                                <div className="view overlay rgba-white-slight">
                                    <img src="https://nsm09.casimages.com/img/2020/07/11/20071111522825017616914584.jpg"   className="img-fluid rounded-bottom" alt="ntsition"/>
                                    <a>
                                        <div className="mask"></div>
                                    </a>
                                </div>
                            </div>

                            <div className="col-md-6 text-left ml-xl-3 ml-lg-0 ml-md-3 mt-3">
                                <h6 className="mb-4">
                                    <Link to={`/actualites/${article.id}`}>
                                        <strong id="titre">
                                            {article.titre}
                                        </strong>
                                    </Link>
                                </h6>
                                <p id="cont" className="dark-grey-text" dangerouslySetInnerHTML={{__html: article.contenu.substring(3, 200) + " ..."}}/>

                                <p>
                                    <strong>
                                        {article.categorie.nom}
                                    </strong>
                                </p>

                                <button className="btn btn-indigo btn-sm" >
                                    <Link to={`/actualites/${article.id}`} id="voir">
                                        Lire plus
                                    </Link>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                ))}
            </div>
        )
    }
}

export default ArticlePostList;