import React from 'react';
import NavBar from "./NavBar";
import Footer from "./Footer";
import {Message} from "./Message";

class GalerieList extends React.Component{
    render(){
        const {photos} = this.props;
        if (null === photos || 0 === photos.length){
            return (<Message message="Aucune photo n'a été trouver, veuillez retourner sur la page d'accueil svp!" />)
        }
        return (
            <div>
                <NavBar/>
                <br/>
                    <section className="container">
                    <div className="mt-5 mb-5">
                        <div className="row">
                            <div className="col-lg-9">
                                <br/>
                                <h1 className="text-center font-weight-bold dark-grey-text py-3">
                                    <strong>Galerie photo</strong>
                                </h1>

                                <hr/>
                                <div className="row">
                                    {photos && photos.map(photo =>(
                                    <div className="col-lg-4">
                                        <div className="card blue-grey mt-1 mb-2">
                                            <div className="card-body">
                                                <div className="view overlay rgba-white-slight">
                                                    <img src={`http://localhost:8000${photo.url}`}  className="img-fluid rounded-bottom" alt="ntsition"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    ))}
                                </div>
                            </div>
                            <div className="col-lg-3">
                                <div className="card blue-grey mt-2 mb-2 my-4">
                                    <div className="card-body">
                                        <h5 className="card-title dark-grey-text text-center grey lighten-4 py-2">
                                            <strong>
                                                NOS PHOTOS ARCHIVÉ
                                            </strong>
                                        </h5>
                                        <div id="carousel-example-4"
                                             className="carousel slide carousel-fade z-depth-1-half"
                                             data-ride="carousel">
                                            <ol className="carousel-indicators">
                                                <li data-target="#carousel-example-4" data-slide-to="0"
                                                    className="active"></li>
                                                <li data-target="#carousel-example-4" data-slide-to="1"></li>
                                                <li data-target="#carousel-example-4" data-slide-to="2"></li>
                                            </ol>

                                            <div className="carousel-inner" role="listbox">
                                                <b>Aucune image</b>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="card blue-grey mt-2 mb-5">
                                    <div className="card-body">
                                        <h5 className="card-title dark-grey-text text-center grey py-1">
                                            <strong>
                                                Rejoignez notre bulletin d'information
                                            </strong>
                                        </h5>
                                        <div className="row mt-2">
                                            <div className="col-md-12">
                                                <div className="input-group md-form form-sm form-3 pl-0">
                                                <span className="input-group-text white black-text" id="basic-addon9">
                                                    @
                                                </span>
                                                    <input type="email"
                                                           className="form-control mt-0 black-border rgba-white-strong"
                                                           placeholder="Email" aria-describedby="basic-addon9"/>
                                                </div>
                                                <button type="button" className="btn btn-grey btn-block mt-4"
                                                        id="Nletter">
                                                    Envoyer
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <Footer/>
            </div>
        )
    }
}

export default GalerieList;