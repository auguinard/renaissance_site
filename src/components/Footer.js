import React from 'react';
import "./footer.css";

export default class Footer extends React.Component {
    render() {
        return(
            <div>
                <footer className="page-footer text-center font-small info-color-dark">
                    <div className="rgba-stylish-strong">
                        <div className="pt-4">
                            <a className="btn btn-outline-white" role="button">
                                Devenir Membre
                                <i className="fas fa-users ml-2"></i>
                            </a>
                        </div>
                        <hr className="my-4"/>
                        <div className="pb-4">
                            <a href="" target="_blank">
                                <i className="fab fa-facebook-f mr-3" id="lienCh"></i>
                            </a>

                            <a href="" target="_blank">
                                <i className="fab fa-twitter mr-3" id="lienCh"></i>
                            </a>

                            <a href="" target="_blank">
                                <i className="fab fa-youtube mr-3" id="lienCh"></i>
                            </a>
                        </div>
                        <div className="footer-copyright py-3" id="lienCh">
                            © 2020 Design by
                            <strong> Init5 Corporation</strong>
                        </div>
                    </div>
                </footer>
            </div>
        );
    }
}