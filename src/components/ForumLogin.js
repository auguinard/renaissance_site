import React from 'react';
import "./ForumLogin.css";
import {reduxForm, Field} from 'redux-form';
import {renderField} from "../forms";
import {connect} from "react-redux";
import {userLoginAttempt} from "../actions/actions";

const mapDispatchToProps = {
    userLoginAttempt
}

class ForumLogin extends React.Component{
    onSubmit(values) {
        console.log(values);
        return this.props.userLoginAttempt(
            values.username,
            values.password
        );
    }
    render(){
        const {handleSubmit} = this.props;
        return (
            <section className="body-lform py-5">
                <br/><br/>
                <div className="container">
                    <div className="d-flex justify-content-center">
                        <div className="user_card">
                            <div className="text-center">
                                <h5>Saisissez vos identifiants de connexion</h5>
                            </div>
                            <div className="d-flex justify-content-center form_container">

                                <form name="form" onSubmit={handleSubmit(this.onSubmit.bind(this))}>
                                    <Field name="username" type="text" placeholder="Nom d'utilisateur" component={renderField} />
                                    <Field name="password" type="password" placeholder="Mot de passe" component={renderField}/>

                                    <div className="d-flex justify-content-center mt-4 login_container">
                                        <button type="submit" className="btn login_btn">
                                            s'identifier
                                        </button>
                                    </div>
                                </form>

                            </div>

                            <div className="mt-4">
                                <div className="d-flex justify-content-center">
                                    <a href="" className="text-link">Mot de passe oublié?</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br/>
            </section>
        )
    }
}

export default reduxForm({
    form: 'ForumLogin'
})(connect(null, mapDispatchToProps)(ForumLogin));