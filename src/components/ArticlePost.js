import React from 'react';
import {Message} from "./Message";
import NavBar from "./NavBar";
import Footer from "./Footer";
import dateFormat from 'dateformat';

export class ArticlePost extends React.Component{
    render() {
        const {article} = this.props;

        if (null === article){
            return (<Message message="Désolé nous n'avons pas trouver votre article" />);
        }
        console.log(article);
        return (
            <div>
                <NavBar/>
                <section className="container">
                    <div className="mt-5 mb-5">
                        <div className="row">
                            <div className="col-lg-9">
                                <div className="mt-5">
                                    <section className="text-justify">

                                        <p className="font-weight-bold">
                                            {article.titre}
                                        </p>

                                        <figure className="figure">
                                            <img src="https://nsm09.casimages.com/img/2020/07/11/20071111522825017616914584.jpg"
                                                 className="figure-img img-fluid z-depth-1"
                                                 alt="mon fghg"/>
                                            <figcaption className="figure-caption">
                                                Actualité
                                            </figcaption>
                                        </figure>

                                            <p dangerouslySetInnerHTML={{__html: article.contenu }}/>


                                        <hr/>

                                        <p className="my-4 font-weight-bold">
                                            Publier le &nbsp;
                                            {dateFormat(`${article.dateAjout}`, "dd-mm-yyyy")}
                                        </p>
                                    </section>
                                </div>
                            </div>
                            <div className="col-lg-3">
                                <div className="card blue-grey mt-5 mb-2">
                                    <div className="">
                                        <h5 className="dark-grey-text text-center mt-2 mb-1 font-weight-bold">
                                            <strong>CATÉGORIES</strong>
                                        </h5>
                                        <ul className="list-group z-depth-1">
                                            <li className="list-group-item d-flex justify-content-between align-items-center">
                                                <a>Politique</a>
                                                <span className="badge indigo badge-pill">4</span>
                                            </li>
                                            <li className="list-group-item d-flex justify-content-between align-items-center">
                                                <a>Économie</a>
                                                <span className="badge indigo badge-pill">4</span>
                                            </li>

                                            <li className="list-group-item d-flex justify-content-between align-items-center">
                                                <a>Enquête</a>
                                                <span className="badge indigo badge-pill">4</span>
                                            </li>

                                            <li className="list-group-item d-flex justify-content-between align-items-center">
                                                <a>Education</a>
                                                <span className="badge indigo badge-pill">4</span>
                                            </li>

                                        </ul>
                                    </div>
                                </div>

                                <div className="card blue-grey mt-2 mb-2 my-4">
                                    <div className="card-body">
                                        <h5 className="card-title dark-grey-text text-center grey lighten-4 py-2">
                                            <strong>
                                                Revue de presse ivoirienne
                                            </strong>
                                        </h5>
                                        <div id="carousel-example-4"
                                             className="carousel slide carousel-fade z-depth-1-half"
                                             data-ride="carousel">
                                            <ol className="carousel-indicators">
                                                <li data-target="#carousel-example-4" data-slide-to="0"
                                                    className="active"></li>
                                                <li data-target="#carousel-example-4" data-slide-to="1"></li>
                                                <li data-target="#carousel-example-4" data-slide-to="2"></li>
                                            </ol>

                                            <div className="carousel-inner" role="listbox">
                                                <div className="carousel-item active">
                                                    <div className="view">
                                                        <img
                                                            src="https://nsm09.casimages.com/img/2020/07/05/20070502253225536216904708.jpg"
                                                            className="img-fluid" alt="jjd"/>
                                                        <a href="">
                                                            <div className="mask flex-center rgba-black-light"></div>
                                                        </a>
                                                    </div>
                                                </div>

                                                <div className="carousel-item">
                                                    <div className="view">
                                                        <img
                                                            src="https://nsm09.casimages.com/img/2020/07/05/20070502253325536216904709.jpg"
                                                            className="img-fluid" alt="djjd"/>
                                                        <a href="">
                                                            <div className="mask flex-center rgba-black-light"></div>
                                                        </a>
                                                    </div>
                                                </div>

                                                <div className="carousel-item">
                                                    <div className="view">
                                                        <img
                                                            src="https://nsm09.casimages.com/img/2020/07/05/20070502253325536216904710.jpg"
                                                            className="img-fluid" alt=" jh"/>
                                                        <a href="">
                                                            <div className="mask flex-center rgba-black-light"></div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>

                                            <a className="carousel-control-prev" href="#carousel-example-4"
                                               role="button" data-slide="prev">
                                                <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                                                <span className="sr-only">Previous</span>
                                            </a>
                                            <a className="carousel-control-next" href="#carousel-example-4"
                                               role="button" data-slide="next">
                                                <span className="carousel-control-next-icon" aria-hidden="true"></span>
                                                <span className="sr-only">Next</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div className="card blue-grey mt-2 mb-5">
                                    <div className="card-body">
                                        <h5 className="card-title dark-grey-text text-center grey py-1">
                                            <strong>
                                                Rejoignez notre bulletin d'information
                                            </strong>
                                        </h5>
                                        <div className="row mt-2">
                                            <div className="col-md-12">
                                                <div className="input-group md-form form-sm form-3 pl-0">
                                                <span className="input-group-text white black-text" id="basic-addon9">
                                                    @
                                                </span>
                                                    <input type="email"
                                                           className="form-control mt-0 black-border rgba-white-strong"
                                                           placeholder="Email" aria-describedby="basic-addon9"/>
                                                </div>
                                                <button type="button" className="btn btn-grey btn-block mt-4"
                                                        id="Nletter">
                                                    Envoyer
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <Footer/>
            </div>
        )
    }
}