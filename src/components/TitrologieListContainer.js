import React from 'react';
import { titrologieListFetch} from "../actions/actions";
import {connect} from "react-redux";
import {Spinner} from "./Spinner";
import TitrologieList from "./TitrologieList";


const mapStateToProps = state => ({
    ...state.titrologieList
});

const mapDispatchToProps = {
    titrologieListFetch
}

class TitrologieListContainer extends React.Component{
    componentDidMount() {
        this.props.titrologieListFetch();
        document.title = 'Rénaissance | Titrologies';
    }

    render(){
        const {revues, isFetching} = this.props;
        if (isFetching) {
            return (
                <div className="container mt-5">
                    <br/>
                    <br/>
                    <br/>
                    <Spinner/>
                </div>
            );
        }
        return (
            <TitrologieList revues={revues} />
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(TitrologieListContainer);