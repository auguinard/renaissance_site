import React from 'react';
import "./Header.css";

export default class NavBar extends React.Component {
    render() {
        return(
            <div>
                <nav className="navbar navbar-expand-lg navbar-dark bg-dark shadow fixed-top" id="ren-nav">
                    <div className="container-fluid">
                        <a className="navbar-brand" href="/">
                            <img className="" alt="Logo" src="https://nsm09.casimages.com/img/2020/07/10/20071012072025017616912382.jpg" width="170" height="50"/>
                        </a>
                        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                            <span className="navbar-toggler-icon"></span>
                        </button>
                        <div className="collapse navbar-collapse" id="navbarResponsive">
                            <ul className="navbar-nav ml-auto">
                                <li className="nav-item active">
                                    <a className="nav-link" href="/">Accueil
                                        <span className="sr-only">(current)</span>
                                    </a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" href="/actualites">Actualité</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" href="/titrologie">Titrologie</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" href="/galerie">Galerie</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" href="">Forum</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" href="/contact-us">Contact</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
        );
    }
}