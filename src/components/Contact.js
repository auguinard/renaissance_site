import React from 'react';
import NavBar from "./NavBar";
import Footer from "./Footer";
import "./Contact.css";

class Contact extends React.Component{
    componentDidMount() {
        document.title = 'Rénaissance | Contactez Nous';
    }
    render(){
        return (
            <div>
                <NavBar/>
                <br/>
                <br/>
                <div className="container my-5 mt-5">
                    <section className="contact-section dark-grey-text mb-5">
                        <div className="">
                            <div className="row">
                                <div className="col-lg-8">
                                    <div className="card-body form">
                                        <h3 className="font-weight-bold dark-grey-text mt-4"><i
                                            className="fas fa-envelope pr-2 mr-1"></i>Écrivez-nous:</h3>
                                        <div className="row">
                                            <div className="col-md-6">
                                                <div className="md-form mb-0">
                                                    <input type="text" id="" className="form-control"/>
                                                        <label htmlFor="form-contact-name" className="">
                                                            Nom
                                                        </label>
                                                </div>
                                            </div>

                                            <div className="col-md-6">
                                                <div className="md-form mb-0">
                                                    <input type="text" id="-email" className="form-control"/>
                                                        <label htmlFor="form-contact-email" className="">
                                                            Email
                                                        </label>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="row">
                                            <div className="col-md-12">
                                                <div className="md-form mb-0">
                                                    <textarea id="" className="form-control md-textarea" rows="3">
                                                    </textarea>
                                                    <label htmlFor="form-contact-message">Votre message</label>
                                                    <div className="text-center">
                                                        <button type="submit" id="btn" className="btn btn-md">
                                                            Envoyer
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="col-lg-4">
                                    <div className="card-body contact text-center h-100 white-text">
                                        <h3 className="font-weight-bold my-4 pb-2">Informations de contact</h3>
                                        <ul className="text-lg-left list-unstyled ml-4">
                                            <li>
                                                <p><i className="fas fa-map-marker-alt pr-2"></i>
                                                    94550 Franklin Roosevelt, Chevilly Larue, Paris, FRANCE.
                                                </p>
                                            </li>
                                            <li>
                                                <p><i className="fas fa-phone pr-2"></i> 07 68 18 28 22</p>
                                            </li>
                                            <li>
                                                <p><i className="fas fa-envelope pr-2"></i>infos@renaissance.ci</p>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <Footer/>
            </div>
        )
    }
}

export default Contact;