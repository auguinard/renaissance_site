import React from 'react';
import {galerieListFetch} from "../actions/actions";
import {connect} from "react-redux";
import {Spinner} from "./Spinner";
import GalerieList from "./GalerieList";


const mapStateToProps = state => ({
    ...state.galerieList
});

const mapDispatchToProps = {
    galerieListFetch
}

class GalerieListContainer extends React.Component{
    componentDidMount() {
        this.props.galerieListFetch();
        document.title = 'Rénaissance | Nos photos';
    }

    render(){
        const {photos, isFetching} = this.props;
        if (isFetching) {
            return (
                <div className="container mt-5">
                    <br/>
                    <br/>
                    <br/>
                    <Spinner/>
                </div>
            );
        }
        return (
            <GalerieList photos={photos} />
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(GalerieListContainer);