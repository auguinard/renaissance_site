import React from 'react';
import timeago from 'timeago.js';
import NavBar from "./NavBar";
import {Message} from "./Message";


export class ForumPost extends React.Component{
    render() {
        const {post} =  this.props;

        if (null === post){
            return (<Message message="se sujet n'existe plus" />);
        }

        return (
            <div>
                <NavBar/>
                <br/><br/>
                <div className="container card mb-3 shadow-sm">
                    <div className="card-body">
                        <h2>{post.titre}</h2>
                        <p className="card-text">{post.contenu}</p>
                        <p className="card-text border-top">
                            <small className="text-muted">
                                {timeago().format(post.dateAjout)} poster par&nbsp; {post.auteur.username}
                            </small>
                        </p>
                    </div>
                </div>
            </div>
        )
    }
}