import React from 'react';
import {forumPostFetch, forumPostUnload} from "../actions/actions";
import {connect} from "react-redux";
import {ForumPost} from "./ForumPost";
import {Spinner} from "./Spinner";
import ForumCommentListContainer from "./ForumCommentListContainer";
import Footer from "./Footer";

const mapeStateToProps = state => ({
   ...state.forumPost
});

const mapDispatchToProps = {
    forumPostFetch,
    forumPostUnload
}

class ForumPostContainer extends React.Component{
    componentDidMount() {
        this.props.forumPostFetch(this.props.match.params.id);
        document.title = 'Rénaissance | Détail sur le sujet';
    }

    componentWillUnmount() {
        this.props.forumPostUnload();
    }

    render() {
        const {isFetching, post} = this.props;
        if (isFetching) {
            return (
                <div className="container mt-5">
                    <br/>
                    <br/>
                    <br/>
                    <Spinner/>
                </div>
            );
        }
        return(
                <div>
                    <section className="container">
                        <div className="mt-5 mb-1">
                            <div className="row">
                                <div className="col-lg-8">
                                    <br/>
                                    <nav className="navbar navbar-expand-lg navbar-light bg-light mt-5" >
                                        <b className="font-weight-bold white-text mr-4" href="/forum">
                                            Accueil Forum
                                        </b>
                                        <button className="navbar-toggler" type="button" data-toggle="collapse"
                                                data-target="#navbarSupportedContent1"
                                                aria-controls="navbarSupportedContent1" aria-expanded="false"
                                                aria-label="Toggle navigation">
                                            <span className="navbar-toggler-icon"></span>
                                        </button>
                                        <div className="collapse navbar-collapse" id="navbarSupportedContent1">
                                            <ul className="navbar-nav mr-auto">

                                            </ul>

                                            <form className="search-form" role="search">
                                                <div className="form-group md-form my-0 waves-light">
                                                    <input type="text" className="form-control" placeholder="Recherche..." name='q' />
                                                </div>
                                            </form>
                                        </div>
                                    </nav>
                                    <hr/>
                                    <div>
                                        <ForumPost post={post} />
                                        <div className="text-center">
                                            <h5>Commentaires</h5>
                                        </div>
                                        {post && <ForumCommentListContainer forumPostId={this.props.match.params.id} />}
                                    </div>
                                </div>
                                <div className="col-lg-4">
                                    <div className="card blue-grey mt-5 mb-3">
                                        <h5 className="card-title dark-grey-text text-center grey lighten-4 mt-4">
                                            <strong>
                                                Startistiques
                                            </strong>
                                        </h5>
                                        <div className="card-body">
                                            <div className="card-body">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <Footer/>
                </div>
            )
    }
}

export default connect(mapeStateToProps, mapDispatchToProps)(ForumPostContainer);