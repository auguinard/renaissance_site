import React from "react";
import {Field, reduxForm} from "redux-form";
import {connect} from "react-redux";
import {renderField} from "../forms";
import {commentAdd} from "../actions/actions";

//const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));

const mapDispatchToProps = {
    commentAdd
};

class FcommentForm extends React.Component{
    onSubmit(values) {
        /*return sleep(2000).then(() => {
            throw new SubmissionError({content: 'Invalid comment'});
        })*/
        const {commentAdd, forumPostId} = this.props;
        return commentAdd(values.content, forumPostId)
    }
    render() {
        const {handleSubmit, submitting} = this.props;
        return (
            <div className="card mb-3 mt-3 shadow-sm">
                <div className="card-body">
                    <form onSubmit={handleSubmit(this.onSubmit.bind(this))}>
                        <Field name="content" label="Saisissez votre commentaire:" type="textarea" component={renderField} />
                        <button type="submit" className="btn btn-big btn-block"
                        disabled={submitting}>
                            Ajouter mon commentaire
                        </button>
                    </form>
                </div>
            </div>)
    }
}

export default reduxForm({
    form: 'CommentForm'
})(connect(null, mapDispatchToProps)(FcommentForm));