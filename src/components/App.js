import React from 'react';
import Header from "./Header";
import Main from "./Main";

class App extends React.Component{
    componentDidMount() {
        document.title = 'Rénaissance | Accueil';
    }
    render(){
        return (
            <div>
                <Header/>
                <Main/>
            </div>
        )
    }
}

export default App;