import React from 'react';
import ReactDOM from 'react-dom';
import {applyMiddleware, createStore} from 'redux';
import {Provider} from 'react-redux';
import {ConnectedRouter} from 'react-router-redux';
import {Route, Switch} from 'react-router';
import App from './components/App';
import { createBrowserHistory as createHistory } from 'history';
import reducer from "./reducer";
import ForumLogin from "./components/ForumLogin";
import Contact from "./components/Contact";
import Membre from "./components/Membre";
import ForumPostListContainer from "./components/ForumPostListContainer";
import thunkMiddleware from 'redux-thunk';
import ForumPostContainer from "./components/ForumPostContainer";
import ArticlePostListContainer from "./components/ArticlePostListContainer";
import ArticlePostContainer from "./components/ArticlePostContainer";
import {tokenMiddleware} from "./middleware";
import TitrologieListContainer from "./components/TitrologieListContainer";
import GalerieListContainer from "./components/GalerieListContainer";

const store = createStore(
    reducer,
    applyMiddleware(thunkMiddleware, tokenMiddleware)
);
const history = createHistory();
//const history = createHistory();

ReactDOM.render((
    <Provider store={store}>
        <ConnectedRouter history={history}>
            <Switch>
                <Route path="/forum/:id" component={ForumPostContainer}/>
                <Route path="/actualites/:id" component={ArticlePostContainer}/>
                <Route path="/forum" component={ForumPostListContainer}/>
                <Route path="/actualites/:page?" component={ArticlePostListContainer}/>
                <Route path="/titrologie" component={TitrologieListContainer}/>
                <Route path="/galerie" component={GalerieListContainer}/>
                <Route path="/contact-us" component={Contact}/>
                <Route path="/login-forum" component={ForumLogin}/>
                <Route path="/devenir-membre" component={Membre}/>
                <Route path="/" component={App}/>
            </Switch>
        </ConnectedRouter>
    </Provider>
), document.getElementById('root'));