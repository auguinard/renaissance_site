import {requests} from "../agent";
import {
    ARTICLE_POST_ERROR,
    ARTICLE_POST_LIST_ERROR,
    ARTICLE_POST_LIST_RECEIVED,
    ARTICLE_POST_LIST_REQUEST, ARTICLE_POST_LIST_SET_PAGE,
    ARTICLE_POST_RECEIVED,
    ARTICLE_POST_REQUEST,
    ARTICLE_POST_UNLOAD,
    FORUM_COMMENT_LIST_ERROR,
    FORUM_COMMENT_LIST_RECEIVED,
    FORUM_COMMENT_LIST_REQUEST,
    FORUM_COMMENT_LIST_UNLOAD, FORUM_POST_COMMENT_ADDED,
    FORUM_POST_ERROR,
    FORUM_POST_LIST_ADD,
    FORUM_POST_LIST_ERROR,
    FORUM_POST_LIST_RECEIVED,
    FORUM_POST_LIST_REQUEST,
    FORUM_POST_RECEIVED,
    FORUM_POST_REQUEST,
    FORUM_POST_UNLOAD, FORUM_REPLY_LIST_ERROR,
    FORUM_REPLY_LIST_RECEIVED, FORUM_REPLY_LIST_REQUEST,
    FORUM_REPLY_LIST_UNLOAD,
    GALERIE_ERROR,
    GALERIE_RECEIVED,
    GALERIE_REQUEST,
    TITROLOGIE_ERROR,
    TITROLOGIE_RECEIVED,
    TITROLOGIE_REQUEST,
    USER_LOGIN_SUCCESS
} from "./constants";

export const forumPostListRequest = () => ({
    type: FORUM_POST_LIST_REQUEST,
});
export const forumPostListError = (error) => ({
    type: FORUM_POST_LIST_ERROR,
    error
});
export const forumPostListReceived = (data) => ({
    type: FORUM_POST_LIST_RECEIVED,
    data
});
export const forumPostListFetch = () => {
    return (dispatch) => {
        dispatch(forumPostListRequest());
        return requests.get('/forum_posts')
            .then(response => dispatch(forumPostListReceived(response)))
            .catch(error => dispatch(forumPostListError(error)));
    }
}

export const articlePostListRequest = () => ({
    type: ARTICLE_POST_LIST_REQUEST,
});
export const articlePostListError = (error) => ({
    type: ARTICLE_POST_LIST_ERROR,
    error
});
export const articlePostListReceived = (data) => ({
    type: ARTICLE_POST_LIST_RECEIVED,
    data
});

export const articlePostListSetPage = (page) => ({
    type: ARTICLE_POST_LIST_SET_PAGE,
    page
});

export const articlePostListFetch = (page = 1) => {
    return (dispatch) => {
        dispatch(articlePostListRequest());
        return requests.get(`/articles?_page=${page}`)
            .then(response => dispatch(articlePostListReceived(response)))
            .catch(error => dispatch(articlePostListError(error)));
    }
}

export const titrologieListRequest = () => ({
    type: TITROLOGIE_REQUEST,
});
export const titrologieListError = (error) => ({
    type: TITROLOGIE_ERROR,
    error
});
export const titrologieListReceived = (data) => ({
    type: TITROLOGIE_RECEIVED,
    data
});

export const titrologieListFetch = () => {
    return (dispatch) => {
        dispatch(titrologieListRequest());
        return requests.get('/revues')
            .then(response => dispatch(titrologieListReceived(response)))
            .catch(error => dispatch(titrologieListError(error)));
    }
}

export const galerieListRequest = () => ({
    type: GALERIE_REQUEST,
});
export const galerieListError = (error) => ({
    type: GALERIE_ERROR,
    error
});
export const galerieListReceived = (data) => ({
    type: GALERIE_RECEIVED,
    data
});

export const galerieListFetch = () => {
    return (dispatch) => {
        dispatch(galerieListRequest());
        return requests.get('/nos_images')
            .then(response => dispatch(galerieListReceived(response)))
            .catch(error => dispatch(galerieListError(error)));
    }
}

export const forumPostRequest = () => ({
    type: FORUM_POST_REQUEST,
});

export const forumPostError = (error) => ({
    type: FORUM_POST_ERROR,
    error
});
export const forumPostReceived = (data) => ({
    type: FORUM_POST_RECEIVED,
    data
});

export const forumPostUnload = () => ({
    type: FORUM_POST_UNLOAD,
});

export const forumPostFetch = (id) => {
    return (dispatch) => {
        dispatch(forumPostRequest());
        return requests.get(`/forum_posts/${id}`)
            .then(response => dispatch(forumPostReceived(response)))
            .catch(error => dispatch(forumPostError(error)));
    }
};

export const articlePostRequest = () => ({
    type: ARTICLE_POST_REQUEST,
});

export const articlePostError = (error) => ({
    type: ARTICLE_POST_ERROR,
    error
});
export const articlePostReceived = (data) => ({
    type: ARTICLE_POST_RECEIVED,
    data
});

export const articlePostUnload = () => ({
    type: ARTICLE_POST_UNLOAD,
});



export const articlePostFetch = (id) => {
    return (dispatch) => {
        dispatch(articlePostRequest());
        return requests.get(`/articles/${id}`)
            .then(response => dispatch(articlePostReceived(response)))
            .catch(error => dispatch(articlePostError(error)));
    }
};

export const forumCommentListRequest = () => ({
    type: FORUM_COMMENT_LIST_REQUEST,
});

export const forumCommentListError = (error) => ({
    type: FORUM_COMMENT_LIST_ERROR,
    error
});
export const forumCommentListReceived = (data) => ({
    type: FORUM_COMMENT_LIST_RECEIVED,
    data
});

export const forumCommentListUnload = () => ({
    type: FORUM_COMMENT_LIST_UNLOAD,
});

export const forumCommentListFetch = (id) => {
    return (dispatch) => {
        dispatch(forumCommentListRequest());
        return requests.get(`/forum_posts/${id}/forum_comments`)
            .then(response => dispatch(forumCommentListReceived(response)))
            .catch(error => dispatch(forumCommentListError(error)));
    }
};

export const forumReplyListRequest = () => ({
    type: FORUM_REPLY_LIST_REQUEST,
});

export const forumReplyListError = (error) => ({
    type: FORUM_REPLY_LIST_ERROR,
    error
});
export const forumReplyListReceived = (data) => ({
    type: FORUM_REPLY_LIST_RECEIVED,
    data
});

export const forumReplyListUnload = () => ({
    type: FORUM_REPLY_LIST_UNLOAD,
});

export const forumReplyListFetch = (id) => {
    return (dispatch) => {
        dispatch(forumReplyListRequest());
        return requests.get(`/forum_comments/${id}/replies`)
            .then(response => dispatch(forumReplyListReceived(response)))
            .catch(error => dispatch(forumReplyListError(error)));
    }
};
export const commentAdded = (comment) => ({
    type: FORUM_POST_COMMENT_ADDED,
    comment
});
export const commentAdd = (comment, forumPostId) => {
    return (dispatch) => {
        return requests.post(
            '/forum_comments',
            {
                content: comment,
                auteur: '/api/auteurs/1',
                forumPost: `api/forum_post/${forumPostId}`
            }, true
        ).then(response => dispatch(commentAdded(response)))
    }
};
//----------//
export const userLoginSuccess = (token, userId) => {
    return {
        type: USER_LOGIN_SUCCESS,
        token,
        userId
    }
};

export const userLoginAttempt = (username, password) => {
    return (dispatch) => {
        return requests.post('/login_check', {username, password}, true).then(
            response => dispatch(userLoginSuccess(response.token, response.id))
        ).catch(error => {
            console.log('Connexion échoué');
        });
    }
};

export const forumPostAdd = () => ({
    type: FORUM_POST_LIST_ADD,
    data: {
        id: Math.floor(Math.random() * 100 + 3),
        titre: 'Un nouvel ajout'
    }
});